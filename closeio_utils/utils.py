from closeio_api import Client
from django.conf import settings
from datetime import datetime

ACTIONS = (
    'subscribe',
    'unsubscribe',
    'signup',
    'contact',
    'newvidzio',
    'newvideo',
    'library_opened',
    'editor_opened',
    'project_saved',
    'vidzio_published'
)

api = Client(settings.CLOSEIO_APIKEY)


def update_or_create(email, action):
    if action in ACTIONS:
        lead_results = query_leads(email)
        id = get_lead_id(lead_results)
        status_label = None

        for lead in lead_results:
            if lead['id'] == id:
                status_label = lead['status_label']

        if id:
            update_lead(id, action, status_label)
        else:
            create_lead(email, action)
    else:
        raise Exception('CMS2.CLOSEIO: BAD ACTION')


# updates and existing lead
def update_lead(id, action, status_label=None):
    data = {}
    if action == 'subscribe':
        data['custom.Newsletter'] = 'subscribe'
        if not status_label == "Qualified":
            data['status'] = "Potential"
    elif action == 'unsubscribe':
        data['custom.Newsletter'] = 'unsubscribe'
        if not status_label == "Qualified":
            data['status'] = "Bad Fit"
    elif action == 'signup':
        data['custom.Approach'] = 'sign up'
        data['status'] = "Qualified"
    elif action == 'contact':
        data['custom.Contacted'] = 'web form'
        if not status_label == "Qualified":
            data['status'] = "Potential"
    else:
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if action == 'newvidzio':
            data['custom.Vidzio Created'] = date
        elif action == 'newvideo':
            data['custom.Video Uploaded'] = date
        elif action == 'library_opened':
            data['custom.Library Opened'] = date
        elif action == 'editor_opened':
            data['custom.Editor Opened'] = date
        elif action == 'project_saved':
            data['custom.Project Saved'] = date
        elif action == 'vidzio_published':
            data['custom.Vidzio Published'] = date

    lead = api.put('lead/%s' % id, data)

    if lead['id']:
        return True
    else:
        raise Exception('CMS2.CLOSEIO: LEAD UPDATE WENT WRONG')


# creates a new lead
def create_lead(email, action):
    data = {}
    update_too = None
    if action == 'subscribe':
        data['custom'] = {'Newsletter': 'subscribe'}
    elif action == 'unsubscribe':
        data['custom'] = {'Newsletter': 'unsubscribe'}
    elif action == 'signup':
        data['custom'] = {'Approach': 'sign up'}
        data['status'] = "Qualified"
    elif action == 'contact':
        data['custom'] = {'Contacted': 'web form'}
    else:
        data['custom'] = {'Approach': 'sign up'}
        data['status'] = "Qualified"
        update_too = True

    data.update({
        'name': email,
        'contacts': [{
          'emails': [{
              'type': 'office',
              'email': email
          }]
        }]
    })

    lead = api.post('lead', data)

    if lead['id']:
        if update_too:
            update_lead(lead['id'], action)
        else:
            return True
    else:
        raise Exception('CMS2.CLOSEIO: LEAD CREATION WENT WRONG')


# fetch multiple leads (using search syntax)
def query_leads(email):
    query_string = 'email: "%s"' % email
    lead_results = api.get('lead', data={
        '_fields': 'id,display_name,status_label',
        'query': query_string
    })
    return lead_results['data']


# return a single lead id even if there are more leads with the same email
def get_lead_id(lead_results):
    if len(lead_results) == 0:
        return None
    elif len(lead_results) > 1:
        return pick_one(lead_results)
    else:
        return lead_results[0]['id']


# picks one appropriate lead among the results
def pick_one(lead_results):
    result = None
    for lead in lead_results:
        if lead['status_label'] == "Qualified":
            return lead['id']
        if lead['status_label'] == "Bad Fit":
            result = lead['id']
    return result if result else lead_results[0]['id']