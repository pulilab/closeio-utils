from mock import Mock, patch
from datetime import datetime
import closeio_api
from django.conf import settings
from django.test import TestCase
from ..utils import pick_one, get_lead_id
from .. import utils

ACTIONS = (
    'bad_fit',
    'qualified',

)

LEAD_RESULTS = {
    'data': [
        {
            "status_label": "Bad Fit",
            "display_name": "TIME TEST 2",
            "id": "lead_weUyYNwRmpTf6jvlM"
        },
        {
            "status_label": "Potential",
            "display_name": "TIME TEST",
            "id": "lead_AEcMNBMVt7rpLH5"
        },
        {
            "status_label": "Qualified",
            "display_name": "TIME TEST",
            "id": "lead_AEcMNBMVt7rpLH5rP2"
        }
    ]
}


def mock_get(action):
    if action in ['bad_fit', 'qualified']:
        return LEAD_RESULTS

def mock_post(*args, **kwargs):
    lead = {}
    lead['id'] = 'test'
    return lead


class CloseIOTest(TestCase):

    def setUp(self):
        self.api = closeio_api.Client(settings.CLOSEIO_APIKEY)
        self.api.get = mock_get
        closeio_api.Client.post = mock_post

    def test_pick_qualified(self):
        lead_results = self.api.get('qualified')
        id = pick_one(lead_results['data'])

        self.assertEquals(id, 'lead_AEcMNBMVt7rpLH5rP2')

    def test_pick_bad_fit(self):
        lead_results = self.api.get('bad_fit')
        id = pick_one(lead_results['data'])

        # Since qualified has presendence over bad_fit, it must return
        # the qualified result instead of bad_fit
        self.assertEquals(id, 'lead_AEcMNBMVt7rpLH5rP2')

        # When we remove the qualified result it should return the bad_fit id though
        id = pick_one(lead_results['data'][0:1])
        self.assertEquals(id, 'lead_weUyYNwRmpTf6jvlM')

    def test_get_lead_one(self):
        lead_results = self.api.get('qualified')
        id = get_lead_id([lead_results['data'][1]])

        self.assertEquals(id, 'lead_AEcMNBMVt7rpLH5')

    def test_get_lead_id_with_pick(self):
        lead_results = self.api.get('qualified')
        id = pick_one(lead_results['data'])

        id2 = get_lead_id(lead_results['data'])

        self.assertEquals(id, id2)

    @patch.object(utils, 'update_lead')
    def test_create_lead_with_new_action_calls_update(self, mocked_update_lead):
        utils.create_lead('test@testtest.com', 'newvideo')
        self.assertTrue(mocked_update_lead.called)

    @patch.object(closeio_api.Client, 'put', return_value={"id": 'test'})
    def test_new_action_sets_up_a_proper_date(self, mocked_put):
        utils.update_lead('test', 'newvideo')
        called_date_dict = mocked_put.call_args[0][1]
        called_date = called_date_dict.values()[0]
        date_obj = datetime.strptime(called_date, "%Y-%m-%d %H:%M:%S")

        self.assertTrue(mocked_put.called)
        self.assertTrue(isinstance(date_obj, datetime))