closeio-utils
========================

This package has some helper functions for updating and creating Leads in close.io. It communicates
with the closeio python API, that can be found here:
https://github.com/elasticsales/closeio-api

Usage
------

1. import package
2. create a signal handler or a handler function for your actions
3. use the `update_or_create(email, action)` method

* Pick an action from here:
ACTIONS = (
    'subscribe',
    'unsubscribe',
    'signup',
    'contact',
    'newvidzio',
    'newvideo',
    'library_opened',
    'editor_opened',
    'project_saved',
    'vidzio_published'
)

Running the standalone tests
----------------------------

	python runtests.py